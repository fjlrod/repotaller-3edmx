var clientesObtenidos;
function getClientes() {
  var url= "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      console.table(JSON.parse(request.responseText).value);
      clientesObtenidos = request.responseText;
      procesarCustomers();
    }
  }
  request.open("GET", url, true);
  request.send();
}
function procesarCustomers (){
 var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
 var JSCustomers = JSON.parse(clientesObtenidos);
 //alert(JSONProducts.value[0].ProductName);
 var divTabla = document.getElementById("divTabla");
 var table = document.createElement("table");
 var tbody =  document.createElement("tbody");
 table.classList.add("table")
 table.classList.add("table.striped");

 for(var i = 0; i < JSCustomers.value.length; i++){
   var nuevaFila = document.createElement("tr");
   var columnaNombre = document.createElement("td");
   columnaNombre.innerText = JSCustomers.value[i].ContactName;

   var columnaCiudad = document.createElement("td");
   columnaCiudad.innerText = JSCustomers.value[i].City;

   var columnaBandera = document.createElement("td");
   var imgBandera = document.createElement("img");

   if(JSCustomer.value[i].Country == "UK"){
      imgBandera.src = rutaBandera + "United-Kingdom.png";
   }else{
      imgBandera.src = rutaBandera + JSCustomers.value[i].Country + ".png";
   }
   columnaBandera.appendChild(imgBandera);

   nuevaFila.appendChild(columnaNombre);
   nuevaFila.appendChild(columnaCiudad);
   nuevaFila.appendChild(columnaBandera);

   tbody.appendChild(nuevaFila);

 }
 table.appendChild(tbody);
 divTabla.appendChild(table);
}
